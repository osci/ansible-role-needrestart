# Ansible role that deploys needrestart

## Introduction

[needrestart](https://github.com/liske/needrestart) checks which daemons need
to be restarted after library upgrades.

This role is made to work in tandem with the `unattended_updates` role. After
the later installs upgrades automagically, the former ensures all services that
were upgraded, or whose dependencies were upgraded (including interpreted
programming languages), are properly restarted.

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role needrestart

```

## Custom Configuration

It is possible to drop configuration bits in `/etc/needrestart/conf.d/`, which
is a good way to exclude services which cannot be safely restarted this way.
